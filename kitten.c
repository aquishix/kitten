#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>

const int MEGABYTE = 1048576;

int main(int argc, char* argv[]) {
    int true = 1; // false!
    char buf[MEGABYTE];
    FILE* file;
    ssize_t read_count;
    
    int i = 1;
    for (; i < argc; i++) {
        file = fopen(argv[i], "r");
        if (file == NULL) {
            perror(argv[1]);
            return EXIT_FAILURE;
        }
        
        read_count = fread(buf, 1, MEGABYTE, file);
        if (read_count == 0 && !feof(file)) {
            fprintf(stderr, "%s: read failed: %s\n",argv[1], strerror(errno));
        }

        fwrite(buf, 1, read_count, stdout);
        fclose(file);
    } 

    return EXIT_SUCCESS;
}
